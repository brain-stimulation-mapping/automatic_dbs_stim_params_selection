README.md

**About the prototype**

The final goal of this MATLAB/LeadDBS addon is to reduce the time patients and clinicians spend tediously testing various DBS parameter values, as well as to improve patient outcomes through finding as optimal parameter values as possible on a patient-specific basis. This GUI and algorithm prototype is a first step towards trying to reach that goal, with a primary focus on suggesting electrode contacts and stimulation currents.

**Installation**

*Download the source code*

The optimization code can be downloaded and installed through the install menu of Lead-DBS. Alternatively, clone or download the code, e.g., via HTTPS from 

<https://gitlab.switch.ch/brain-stimulation-mapping/automatic_dbs_stim_params_selection.git>


**Demo patient case**

*Install the GUI app*

In Matlab, go to the cloned repository's subfolder "optimization" and double-click on the file named "DBS_Stimulation_Optimization.mlappinstall". In the tab "Apps" in Matlab, an icon with the name DBS Stimulation Optimization should appear. Click on the app to open the GUI with a demo patient. Press "Run optimization". 

Make sure to have added the optimization folder and LeadDBS to your Matlab search path.



**Using your own patients**

*Step 1: Render a patient with LeadDBS.*

*Step 2: In the toolbar in the electrode-scene figure, there should be an icon with the name: Automatic Stimulation Parameter Selection.*

Press that icon to open the optimization GUI.


**Feedback**

Feel free to evaluate and leave feedback on the prototype and algorithm using the following form:

<https://docs.google.com/forms/d/e/1FAIpQLSfEDSY-BqhvEFVHyZa7XO--pR_dplub44xPlw4_nECMwFf0VQ/viewform>

You can also reach me directly via e-mail at simon.nordenstrom@gmail.com

**Requirements**

MATLAB v. R2020b (or later?)
