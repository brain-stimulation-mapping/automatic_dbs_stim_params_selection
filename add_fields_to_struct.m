function newStruct = add_fields_to_struct(origStruct, fieldStruct)
% adds the fields in fieldStruct to origStruct, without removing original
% fields

newFields = fieldnames(fieldStruct);

for i = 1:numel(newFields)
    origStruct.(newFields{i}) = fieldStruct.(newFields{i});
end 

newStruct = origStruct; 

end