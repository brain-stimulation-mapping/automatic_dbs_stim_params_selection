function varargout = autostim_main(varargin)
% autostim_main is the main script for
% generating stimparams suggestions. It gets
% its inputs from the autostimulationGUI app.

autostimTimer = tic;



%% 1) get input arguments

%TODO: check nmbr of input arguments, errors etc
elstruct = varargin{1}; %or use ea_load_reconstruction?
resultfig = varargin{2}; 
options = varargin{3};
GUI_settings = varargin{4};


%% 2) initialize some variables from input arguments


patientFolder = options.uipatdirs{1};
coords_mm = elstruct.coords_mm;

stimLabelBase = GUI_settings.basic.stimLabelBase;
sp = GUI_settings.basic.sp; %Sweetspot files
optimalOR = GUI_settings.advanced.optimalOR; %Desired overlap ratio 

stimType = lower(GUI_settings.advanced.stimType); 
minAmp = round(GUI_settings.advanced.minAmp,1); %Min current amp selected by user
maxAmp = round(GUI_settings.advanced.maxAmp,1);
stepSize = GUI_settings.advanced.stepSize; 
estimateInTemplate = GUI_settings.advanced.estInTemp;
ringStim = GUI_settings.advanced.ringStim; % consider directional electrodes together
optiMethod = GUI_settings.advanced.optiMethod; %IAS or SimBio/overlap calc
optiAlgo = GUI_settings.advanced.optiAlgo; %Brute force/golden section...
evalMetric = GUI_settings.advanced.evalMetric; %By default perc of sweetspot stimulated
minOverlapVol.L = GUI_settings.advanced.minOverlapVol.L; %min vol of SS activated for full effect
minOverlapVol.R = GUI_settings.advanced.minOverlapVol.R;
maxNonOverlapVol.L = GUI_settings.advanced.maxNonOverlapVol.L; %for side-effects
maxNonOverlapVol.R = GUI_settings.advanced.maxNonOverlapVol.R;


minSTNOverlapPerc = GUI_settings.advanced.minSTNOverlapPerc; %eval metric

OLRMaxBool = GUI_settings.advanced.OLRMaxBool; %display overlap ratios at suggested currents (0 by default)

%For generating VTAs from recon files in earlier Lead-DBS version with
%CW instead of CCW convention (will not work on other computers)
if strcmp(options.root, '/home/brainstimmaps/20xx_Projects/2100_SimonFirstProject/03_Data/CohortBern01ForSimonTest/') ...
        || strcmp(options.root,'/media/brainstimmaps/DATA/03_Data/CohortBern01ForSimonTest/')
    CWConvention = 1; 
else
    CWConvention = 0;
end

rightHemisphere = ~isempty(sp.Name.Right);
leftHemisphere  = ~isempty(sp.Name.Left);

%% 3. Load and get volume of sweetspot

%Right hemisphere
if rightHemisphere
    elReco.sweetspot.pathR{1} = fullfile(sp.Path.Right,sp.Name.Right);

    [volume_spR, spNiiR] = calc_vol(elReco.sweetspot.pathR{1});
    
    %Convert perc of SS activation to vol of SS activation
    if strcmp(evalMetric, "Minimum percentage of sweetspot activation")
        minOverlapVol.R = round(volume_spR*minSTNOverlapPerc.R/100);
    end
end

%Left hemisphere
if leftHemisphere
    elReco.sweetspot.pathL{1} = fullfile(sp.Path.Left,sp.Name.Left);

    [volume_spL, spNiiL] = calc_vol(elReco.sweetspot.pathL{1});
    
    %Convert perc of SS activation to vol of SS activation
    if strcmp(evalMetric, "Minimum percentage of sweetspot activation")
        minOverlapVol.L = round(volume_spL*minSTNOverlapPerc.L/100);
    end
end


%% 4. Get 3D coords of sweetspot in global coord system

%Right hemisphere
if rightHemisphere
    coordsSpmmR =  img_vox2mm(spNiiR);
end

%Left hemisphere
if leftHemisphere
    coordsSpmmL =  img_vox2mm(spNiiL);
end

%% 5. Calculate distance from center of each electrode to mean of sweetspot

%Right
figs.elSpDistR = figure; %figs struct will be sent back to GUI

if rightHemisphere
    
    side = 1;
    mean_sweetspot_coordsR = mean(coordsSpmmR); %CoM of SS if binarized
    [dist_sptoelecR, min_distR] = dist_sweetspot_elecs(mean_sweetspot_coordsR, coords_mm{side});

    elReco.DistR = dist_sptoelecR; %the elReco struct will contain most of the results

    plot(0:7,dist_sptoelecR,'*')
end

xlabel('Electrode number (0 bottom, 7 top)')
ylabel('Distance to mean of sweetspot [mm]')
title('Electrode-sweetspot distance, right')



%Left
figs.elSpDistL = figure;


if leftHemisphere
    side = 2;
    
    mean_sweetspot_coordsL = mean(coordsSpmmL);
    [dist_sptoelecL, min_distL] = dist_sweetspot_elecs(mean_sweetspot_coordsL, coords_mm{side});

    elReco.DistL = dist_sptoelecL;
    plot(0:7,dist_sptoelecL,'*')

end

xlabel('Electrode number (0 bottom, 7 top)')
ylabel('Distance to mean of sweetspot [mm]')
title('Electrode-sweetspot distance, left')

%% 6. Select the (three?) electrodes closest to the sweetspot
% NB: all electrodes are used if using the IAS model, but to save time with
% VTA generation, only three are used with Simbio

if rightHemisphere
    nmbrSelElR = 3; %TODO possibility to change from GUI input?
    [selElecsIndR, minDevElR] = select_elecs(nmbrSelElR, elReco.DistR);
end

if leftHemisphere
    nmbrSelElL = 3; %TODO possibility to change from GUI input?
    [selElecsIndL, minDevElL] = select_elecs(nmbrSelElL, elReco.DistL);
end


%% 7. Optimize current amplitude

    
modelAllBoolR = 1; %Model all electrodes (for IAS)
modelAllBoolL = 1;

stimLabelBaseR = [stimLabelBase, 'R_'];
stimLabelBaseL = [stimLabelBase, 'L_'];

binary = 1; %for ea_nii_overlap
threshold = 0.2; %for ea_nii_overlap

%Use SimBio and overlap calc for optimization
if (strcmp(optiMethod, 'SimBio VTA and overlap calculation (Horn et al.)'))

    
    %Right 
    if rightHemisphere

        anode = -1; %case
        side = 'right';
        cathodeR = selElecsIndR;
        sweetSpotPathR = elReco.sweetspot.pathR{1};
        


        switch optiAlgo
            case 'Brute force VTA generation'
                
                %Settings for VTA generation
                side = 'r';
                if ~ringStim
                    cathodeR = selElecsIndR;
                else
                    %elec 2 and 5 contain the ring stimulations
                    cathodeR = [0 2 5 7]; % change this for time reduction?
                end

                stimAmplitudeR = [1:4 5.1  6:8];
                
                %Generate VTAs for selected electrodes
                rightTimer = tic;
                outer_VTA_wrapper(patientFolder, stimLabelBaseR, ...
                    side, cathodeR, stimAmplitudeR, anode, stimType, estimateInTemplate,CWConvention, ringStim)
                rightTimer = toc(rightTimer);
                disp(['Elapsed time for right hemisphere VTA calc.:', num2str(rightTimer),' s'])
                
                addpath(genpath(patientFolder))
                
                %Calculate overlap ratios, overlap & non-overlap volumes
                %and min (optimal) and (in the future) max currents
                disp('Calculating overlap volumes and optimal currents, R')
                
                rightTimer = tic;
                side = 1;
                
                [resultStruct, effThresStruct, minDevElR, minCurrentAmpR, maxCurrentAmpR, OLRAtMinCurrR]...
                    = calc_results(ringStim, stimAmplitudeR, side, coords_mm, patientFolder, ...
                    stimLabelBaseR, selElecsIndR, elReco.sweetspot.pathR{1},...
                    minSTNOverlapPerc.R ); %calc results from VTAs generated
                
                if ~isfield(elReco, 'forOpti')
                    elReco.forOpti = [];
                end
                elReco.forOpti = add_fields_to_struct(elReco.forOpti, resultStruct); %add results to struct
                
                if ~isfield(elReco, 'model')
                    elReco.model = [];
                end
                elReco.model = add_fields_to_struct(elReco.model, effThresStruct); %add results to struct
                
                
                rightTimer = toc(rightTimer);
                
                disp(['Elapsed time for right overlap and optimal current calculations:', ...
                    num2str(rightTimer),' s'])
                
                % Plot overlap ratios, overlap & non-overlap volumes results 
                hWithSubR = figure;
                pos = get(hWithSubR,'pos');
                set(hWithSubR,'pos',[pos(1) pos(2) 1500 500])
                
                plot_overlap_results(ringStim, elReco.forOpti.stimAmpR, elReco.forOpti.OLRR,...
                    elReco.forOpti.percSSR, elReco.forOpti.nonOLVolR, ...
                    volume_spR, hWithSubR)
                
                

            %Non-robust to local extrema and slow convergence - not used
            case 'Golden section parabolic interpolation'
                
                optiTimerR = tic;       
                
                minCurrentAmpR = golden_section_optimization(minAmp, minAmp,...
                    binary, threshold, sweetSpotPathR, patientFolder, ...
                    stimLabelBaseR, side, cathodeR(1), anode, stimType, ...
                    estimateInTemplate, optimalOR.R, CWConvention, ringStim)

                optiTimerR = toc(optiTimerR);
                disp(['Elapsed time for right hemisphere optimization:', num2str(optiTimerR), ' s'])
        end
        
        figs.modelR = gcf;
    end

    %Left
    if leftHemisphere

        anode = -1;
        side = 'left';
        cathodeL = selElecsIndL;
        sweetSpotPathL = elReco.sweetspot.pathL{1};
        
        switch optiAlgo
            case 'Brute force VTA generation'
                
                %Settings for VTA generation
                side = 'l';
                if ~ringStim
                    cathodeL = selElecsIndL;
                else
                    cathodeL = [0 2 5 7]; % change this for time reduction?
                end

                stimAmplitudeL = [1:4 5.1  6:8];
                
                %Generate VTAs for selected electrodes
                leftTimer = tic;
                outer_VTA_wrapper(patientFolder, stimLabelBaseL, ...
                    side, cathodeL, stimAmplitudeL, anode, stimType, ...
                    estimateInTemplate,CWConvention, ringStim)
                leftTimer = toc(leftTimer);
                disp(['Elapsed time for left hemisphere VTA calc.:', num2str(leftTimer),' s'])
                
                addpath(genpath(patientFolder))
                
                %Calculate overlap ratios, overlap & non-overlap volumes
                %and min (optimal) and (in the future) max currents
                disp('Calculating overlap volumes and optimal currents, L')
                
                leftTimer = tic;
                side = 2;
                
                [resultStruct, effThresStruct, minDevElL, minCurrentAmpL, maxCurrentAmpL, OLRAtMinCurrL]...
                    = calc_results(ringStim, ...
                    stimAmplitudeL, side, coords_mm, patientFolder, ...
                    stimLabelBaseL, selElecsIndL, elReco.sweetspot.pathL{1},...
                    minSTNOverlapPerc.L);
                
                if ~isfield(elReco, 'forOpti')
                    elReco.forOpti = [];
                end
                elReco.forOpti = add_fields_to_struct(elReco.forOpti, resultStruct);
                
                if ~isfield(elReco, 'model')
                    elReco.model = [];
                end
                elReco.model = add_fields_to_struct(elReco.model, effThresStruct);
                
                
                leftTimer = toc(leftTimer);
                
                disp(['Elapsed time for left overlap and optimal current calculations:', ...
                    num2str(leftTimer),' s'])
                
                % Plot overlap ratios, overlap & non-overlap volumes results 
                hWithSubL = figure;
                pos = get(hWithSubL,'pos');
                set(hWithSubL,'pos',[pos(1) pos(2) 1500 500])
                
                plot_overlap_results(ringStim, elReco.forOpti.stimAmpL, elReco.forOpti.OLRL,...
                    elReco.forOpti.percSSL, elReco.forOpti.nonOLVolL, ...
                    volume_spL, hWithSubL)

            
            %Non-robust to local extrema and slow convergence - not used
            case 'Golden section parabolic interpolation'

                optiTimerL = tic;       
                
                minCurrentAmpL = golden_section_optimization(minAmp, minAmp,...
                    binary, threshold, sweetSpotPathL, patientFolder, ...
                    stimLabelBaseL, side, cathodeL(1), anode, stimType, ...
                    estimateInTemplate, optimalOR.L, CWConvention, ringStim)

                optiTimerL = toc(optiTimerL);
                disp(['Elapsed time for left hemisphere optimization:', num2str(optiTimerL), ' s'])
        end
        figs.modelL = gcf;
    end
    
%Use Instant Analytical Spheres model to optimize
elseif (strcmp(optiMethod, 'Instant analytical spheres (Nordenstroem)'))
    
    speedRatio = 0.1; % directional movement of CoM of directional VTAs
    fromRingCenter = 0; % model initial CoM of directional VTAs from center of ring (from surface by default)

    
    if rightHemisphere
        
        side = 1;
        if ringStim
            elContTot = [0, 2, 5, 7]; %the four levels
        elseif modelAllBoolR
            elContTot = 0:7;
        else
            elContTot = minDevElR; %not used
        end
        

        for elCont = elContTot %TODO num electrode contacts (e.g., not always 8)
            
            [r1_fullR, mA_fullR, OLR_fullR, V_OL_fullR, V_non_OLR] = instant_Analytical_Spheres(volume_spR,...
                    coords_mm,elCont,mean_sweetspot_coordsR, side,ringStim, ...
                    speedRatio, fromRingCenter,modelAllBoolR); %model electrode performance
                
            [minCurrentAmpR, OLRAtMinCurrR, maxOLRR,currentAtMaxOLRR, maxCurrentAmpR, V_OL_at_min_currR, V_non_OL_at_max_currR]...
                    = get_IAS_results_per_elec(evalMetric, minOverlapVol.R, maxNonOverlapVol.R, ...
                           mA_fullR, OLR_fullR, V_OL_fullR, V_non_OLR,...
                       dist_sptoelecR(elCont+1), volume_spR); %extract electrode results from modeling
                       
            %fill elReco.model with results from current electrode 
            elReco.model.minCurrentAmpR{elCont+1} = minCurrentAmpR;
            elReco.model.OLRAtMinCurrR{elCont+1} = OLRAtMinCurrR;
            elReco.model.maxOLRR{elCont+1} = maxOLRR;
            elReco.model.currentAtMaxOLRR{elCont+1} = currentAtMaxOLRR;
            elReco.model.maxCurrentAmpR{elCont+1} = maxCurrentAmpR;

        end
        
        %calculate results for the entire lead 
        [minCurrentAmpR, maxCurrentAmpR, OLRAtMinCurrR ] ...
            = get_IAS_results_for_lead(elReco.model.minCurrentAmpR,...
            elReco.model.maxCurrentAmpR, elReco.model.OLRAtMinCurrR);        
       
        figs.modelR = gcf;
        
    end
    
    if leftHemisphere
        side = 2;
        
        if ringStim
            elContTot = [0, 2, 5, 7];
        elseif modelAllBoolL
            elContTot = 0:7;
        else
            elContTot = minDevElL;
        end       
        

        for elCont = elContTot %TODO num electrode contacts
            
            [r1_fullL, mA_fullL, OLR_fullL, V_OL_fullL, V_non_OLL] = instant_Analytical_Spheres(volume_spL,...
                    coords_mm,elCont,mean_sweetspot_coordsL, side,ringStim, ...
                    speedRatio, fromRingCenter,modelAllBoolL);
                
            [minCurrentAmpL, OLRAtMinCurrL, maxOLRL,currentAtMaxOLRL, maxCurrentAmpL]...
                    = get_IAS_results_per_elec(evalMetric, minOverlapVol.L, maxNonOverlapVol.L, ...
                           mA_fullL, OLR_fullL, V_OL_fullL, V_non_OLL,...
                           dist_sptoelecL(elCont+1), volume_spL);
                       
            elReco.model.minCurrentAmpL{elCont+1} = minCurrentAmpL;
            elReco.model.OLRAtMinCurrL{elCont+1} = OLRAtMinCurrL;
            elReco.model.maxOLRL{elCont+1} = maxOLRL;
            elReco.model.currentAtMaxOLRL{elCont+1} = currentAtMaxOLRL;
            elReco.model.maxCurrentAmpL{elCont+1} = maxCurrentAmpL;

        end
        
        [minCurrentAmpL, maxCurrentAmpL, OLRAtMinCurrL ] ...
            = get_IAS_results_for_lead(elReco.model.minCurrentAmpL,...
            elReco.model.maxCurrentAmpL, elReco.model.OLRAtMinCurrL);

        
        figs.modelL = gcf;
    end
end

%% 8. Display results in command window

%Right 
if rightHemisphere
    side = 'right';
    display_opt_results(ringStim, side, minCurrentAmpR, ...
        maxCurrentAmpR, OLRAtMinCurrR, minDevElR);
end

%Left
if leftHemisphere
    side = 'left';
    display_opt_results(ringStim, side, minCurrentAmpL, ...
        maxCurrentAmpL, OLRAtMinCurrL, minDevElL);
end

%% 9. Plot bar graph of results


figs.barFig = figure;

subplotNmbr = '121';
subplot(subplotNmbr)

if rightHemisphere
    
    %initialize field that might not exist
    if ~any(strcmp(fieldnames(elReco.model),'currentAtMaxOLRR')) || isempty(elReco.model.currentAtMaxOLRR)
        elReco.model.currentAtMaxOLRR = [];
    end
    
    %plot suggested currents in a bar plot
    bar_plot_of_suggestions(side, OLRMaxBool, ringStim, elReco.model.maxCurrentAmpR,...
        elReco.model.minCurrentAmpR, elReco.model.maxCurrentAmpR, ...
        elReco.model.minCurrentAmpR, modelAllBoolR,...
        figs.barFig, subplotNmbr, evalMetric, elReco.model.currentAtMaxOLRR)
end

%set title, legend, axes labels, ticks etc of the figure
bar_plot_meta_info(side, figs.barFig, subplotNmbr, OLRMaxBool, evalMetric, ringStim)

subplotNmbr = 122;
subplot(subplotNmbr)

if leftHemisphere
    if ~any(strcmp(fieldnames(elReco.model),'currentAtMaxOLRL')) || isempty(elReco.model.currentAtMaxOLRL)
        elReco.model.currentAtMaxOLRL = [];
    end
    
    bar_plot_of_suggestions(side, OLRMaxBool, ringStim, elReco.model.maxCurrentAmpL,...
        elReco.model.minCurrentAmpL, elReco.model.maxCurrentAmpL, ...
        elReco.model.minCurrentAmpL, modelAllBoolL, figs.barFig, ...
        subplotNmbr, evalMetric, elReco.model.currentAtMaxOLRL)

end

bar_plot_meta_info(side, figs.barFig, subplotNmbr, OLRMaxBool, evalMetric, ringStim)
%% 10. Print suggestions to .txt file
if rightHemisphere
    side = 'right';
    
    %(txt file will end up in a stim_recommendations folder in stimulations/MNI...)
    txtFile = print_results_to_txt(patientFolder, side, minDevElR,minCurrentAmpR,...
        maxCurrentAmpR, OLRAtMinCurrR, estimateInTemplate);
end

if leftHemisphere
    side = 'left';
    if ~exist('txtFile','var')
        txtFile = [];
    end
    txtFile = print_results_to_txt(patientFolder, side, minDevElL,minCurrentAmpL,...
        maxCurrentAmpL, OLRAtMinCurrL, estimateInTemplate,txtFile);
end
print_settings_to_txt(rightHemisphere, leftHemisphere, stimLabelBase,...
    sp.Name, minOverlapVol, maxNonOverlapVol, estimateInTemplate, ringStim,...
    optiMethod, evalMetric, txtFile)


autostimTimer = toc(autostimTimer);
disp(' ')
disp(['Total elapsed time for optimization algorithm:', num2str(autostimTimer),' s'])

%% 11. Return data to GUI

%initialize figures and legends for GUI if they don't already exist
if ~isfield(figs,'modelL')
    figs.modelL = figure;
    
    for i = 1:3
        subplot(1,3,i); legend('');
    end

end

if ~isfield(figs,'modelR')
    figs.modelR = figure;   
    
    for i = 1:3
        subplot(1,3,i); legend('');
    end

end

dataToGUI.Init = NaN; %initialize dataToGUI struct

if leftHemisphere
    side = 'left';
    dataToGUIPart = data_to_GUI(side,minDevElL,minCurrentAmpL,stimLabelBaseL); %get results data
    dataToGUI = add_fields_to_struct(dataToGUI,dataToGUIPart); %fill dataToGUI with results data
end

if rightHemisphere
    side = 'right';
    dataToGUIPart = data_to_GUI(side,minDevElR,minCurrentAmpR,stimLabelBaseR);
    dataToGUI = add_fields_to_struct(dataToGUI,dataToGUIPart);
end

side = -1;
dataToGUIPart = data_to_GUI(side,patientFolder,stimType,estimateInTemplate, ...
    CWConvention, ringStim); %get settings data

dataToGUI = add_fields_to_struct(dataToGUI,dataToGUIPart); %add settings data to struct
dataToGUI = rmfield(dataToGUI, 'Init'); %remove initialized dummy field


figNames = fieldnames(figs);

%make figures invisible (will show up in GUI instead)
for i = 1:numel(figNames)
    set(figs.(figNames{i}),'Visible','Off')
end

varargout{1} = figs; 
varargout{2} = dataToGUI;


