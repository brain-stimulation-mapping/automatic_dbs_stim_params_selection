function bar_plot_meta_info(side,figureHandle, subplotNmbrText, OLRMaxBool, evalMetric, ringStim)
% set axes labels, title, legend and ticks labels of suggested currents
% bar plot

figure(figureHandle)
subplot(subplotNmbrText)

ylabel('Electrode number [0-7]');
xlabel('Current amplitude [mA]');

switch lower(side)
    case {'r','right',1}
        addSide = 'right';
    case {'l','left',2}        
        addSide = 'left';
end

%title(['Suggested min and max current per electrode, ', addSide]);
title(['Suggested stimulation currents per electrode, ', addSide]);

if OLRMaxBool && strcmp(evalMetric, "Overlap ratio maximization within amplitude range")
    leg = legend('Max current selected','Min current selected', 'Max overlap ratio');
elseif OLRMaxBool 
    leg = legend('Side-effect threshold','Full therapeutic effect threshold', 'Max overlap ratio');
else
    leg = legend('Side-effect threshold','Full therapeutic effect threshold');
end

set(leg,'location','best')

if ringStim
    yticklabels({'0', '1-2-3', ' 4-5-6',' 7'})
end

xlim([0,8])
end