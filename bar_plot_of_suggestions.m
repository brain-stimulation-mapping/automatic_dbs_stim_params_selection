function bar_plot_of_suggestions(side,OLRMaxBool, ringStim, maxCurrentAmpVect,...
    minCurrentAmpVect, maxCurrentAmp, minCurrentAmp, modelAllBool,...
    figureHandle, subplotNmbr, evalMetric, varargin)
%plots suggested currents in a bar plot

if numel(varargin) >= 1
    currentAtMaxOLR = varargin{1};
end

pos = get(figureHandle,'pos');
set(figureHandle,'pos',[pos(1) pos(2) 1500 500])

% check if current for max overlap ratio should be plotted
if OLRMaxBool && exist(currentAtMaxOLR,'var') && ~isempty(currentAtMaxOLR) %Needed?
    nmbrBars = 3;
else
    nmbrBars = 2;
end

plotSideEffThres = 0; %Default: don't plot side-effect thresholds

if ~plotSideEffThres
    if ringStim
        numElem = 4;
    else
        numElem = numel(minCurrentAmpVect);
    end
    maxCurrentAmpVect = num2cell(NaN(1,numElem)); 
end

%if ringstim: only 4 electrodes to plot. If OLRMaxBool, plot max OLR current
if ringStim && ~OLRMaxBool 
    x = 0:3;
    bar = barh(x, [cell2mat(maxCurrentAmpVect); cell2mat(minCurrentAmpVect)]);
elseif ringStim && OLRMaxBool   
    x = 0:3;
    bar = barh(x, [cell2mat(maxCurrentAmpVect); cell2mat(minCurrentAmpVect); ...
        cell2mat(currentAtMaxOLR)]);
elseif modelAllBool && ~OLRMaxBool
    bar = barh(0:7, [cell2mat(maxCurrentAmpVect); cell2mat(minCurrentAmpVect)]);
elseif modelAllBool && OLRMaxBool
    bar = barh(0:7, [cell2mat(maxCurrentAmpVect); cell2mat(minCurrentAmpVect); ...
    cell2mat(currentAtMaxOLR)]);
else
    bar = barh(minDevEl, [maxCurrentAmp, minCurrentAmp]);
end

bar(1).FaceColor = 'r';
bar(2).FaceColor = 'b';
bar(2).EdgeColor = 'magenta';

% position text labels with mA values at tips of bars
for i = 1:nmbrBars
    
    ytips = bar(i).XEndPoints; %y and x switched, since bar plot
    xtips = bar(i).YEndPoints+0.1;

    if i == 3 %add also current at max OLR
        labels = strcat(string(cell2mat(maxOLR)*100),' %');
        ytips = ytips + 0.05;
    else
        labels = string(bar(i).YData); %get mA value
        labels(str2double(labels)>8) = ""; %don't plot outside of graph window
    end

    text(xtips,ytips,labels)
end

%bar_plot_meta_info(side, figureHandle, subplotNmbr, OLRMaxBool, evalMetric, ringStim)

end




