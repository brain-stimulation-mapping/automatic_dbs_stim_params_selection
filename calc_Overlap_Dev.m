function overlapRatioDev = calc_Overlap_Dev(stimAmplitude, ...
                        binary, threshold, sweetSpotPath, ...
                        patientFolder, stimLabelBase, side, cathode, ...
                        anode, stimType, estimateInTemplate, optimalOR,...
                        CWConvention, ringStim)         
% Used with Golden Section algo with desired overlap ratio as eval metric.
% Calculates how much the actual OLR of the current VTA differs from the desired one


stimAmplitude = round(stimAmplitude,1)
%msgbox(num2str(stimAmplitude))
tic
VTACalculationSimon(patientFolder, stimLabelBase, side, cathode, ...
    stimAmplitude, anode, stimType, estimateInTemplate, CWConvention, ...
    ringStim)
toc

switch lower(side)
    case {1, 'r', 'right'}
        VTAPath = [patientFolder, filesep, 'stimulations/MNI_ICBM_2009b_NLIN_ASYM', ...
        filesep, stimLabelBase, num2str(stimAmplitude), 'mA_', num2str(cathode), 'Elec', filesep,'vat_right.nii']
    case {2, 'l', 'left'}
        VTAPath = [patientFolder, filesep, 'stimulations/MNI_ICBM_2009b_NLIN_ASYM', ...
        filesep, stimLabelBase, num2str(stimAmplitude), 'mA_', num2str(cathode), 'Elec', filesep,'vat_left.nii']
end




try
    [overlap1, normOverlap1, overlap2, normOverlap2BestEl] = ea_nii_overlap(sweetSpotPath,...
                VTAPath,binary, threshold);
catch
    disp('error in overlap calculation')
end

overlapRatioDev = abs(normOverlap2BestEl-optimalOR);
end