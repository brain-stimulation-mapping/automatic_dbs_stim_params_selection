
function [resultStruct, effThresStruct, varargout] = calc_results(ringStim, stimAmplitude,...
                side, coordsLeads, patientFolder, stimLabelBase, selElecsInd, SSPath, ...
                minSTNPerc)

%calculates results from SimBio and overlap method. resultStruct contains
%e.g., OLR, stimAmplitude, percentage of SS stimulated. effThresStruct
%contains e.g., effective and side-effect thresholds.
            
nmbrStim = numel(stimAmplitude); %Max number of VTAs generated for one electrode

binary = 1;
threshold = 0.2;

if ~ringStim %all electrodes
    elLoopInd = 1:size(coordsLeads{side},1); 
else %ring level indices
    elLoopInd = [0 2 5 7]+1; 
end

if side == 1
    VTAName = 'vat_right.nii';
elseif side == 2
    VTAName = 'vat_left.nii';
end

for indexEl = elLoopInd %loop over each electrode
    for indexStim = 1:nmbrStim %loop over each VTA generated for that electrode
        try

            if ringStim && indexEl == 2+1 %lower directional ring level
                elecText = '123';
            elseif ringStim && indexEl == 5+1 %upper directional ring level
                elecText = '456';
            else
                elecText = num2str(indexEl-1); %electrode number
            end
            

            specStimAmplitude = stimAmplitude(indexStim); 
            %find VTA
            VTAPath = [patientFolder, filesep, 'stimulations/MNI_ICBM_2009b_NLIN_ASYM', ... %TODO change to depend on native/template
            filesep, stimLabelBase, num2str(specStimAmplitude), 'mA_', elecText, 'Elec', filesep,VTAName];
        catch errObj
            VTAPath = NaN;
            disp(errObj.message)
        end

        % if VTA exists and the electrode is part of the 3 selected elecs,
        % or ring stimulation is selected, calculate overlaps
        if exist(VTAPath,'file') && (ismember(indexEl-1,selElecsInd) || ringStim)
            try
                [overlap1, normOverlap1, overlap2, normOverlap2] = ...
                    ea_nii_overlap_reslice(SSPath,...
                    VTAPath, binary, threshold);  %calc overlaps with SS
                forOpti.OLR{indexEl, indexStim} = normOverlap2; %overlap ratio
                forOpti.percSS{indexEl, indexStim} = normOverlap1; %percentage of SS activated
                
                %load volume of VTA
                if side == 1
                    volPath = [patientFolder, filesep, 'stimulations/MNI_ICBM_2009b_NLIN_ASYM', ... %TODO change to depend on native/template
                            filesep, stimLabelBase, num2str(specStimAmplitude), 'mA_', elecText, 'Elec', filesep, 'vat_right.mat'];
                elseif side == 2
                    volPath = [patientFolder, filesep, 'stimulations/MNI_ICBM_2009b_NLIN_ASYM', ... %TODO change to depend on native/template
                            filesep, stimLabelBase, num2str(specStimAmplitude), 'mA_', elecText, 'Elec', filesep, 'vat_left.mat'];    
                end
                load(volPath, 'vatvolume')
                
                %calculate amount of stimulated volume that's not part of
                %the SS
                forOpti.nonOLVol{indexEl, indexStim} = vatvolume*(1-normOverlap2);
            catch errorObj
                forOpti.OLR{indexEl, indexStim} = NaN;
                forOpti.percSS{indexEl, indexStim} = NaN;
                forOpti.nonOLVol{indexEl, indexStim} = NaN;
                warning([errorObj.message, ', filling with NaNs'])
            end
        else
                forOpti.OLR{indexEl, indexStim} = NaN;
                forOpti.percSS{indexEl, indexStim} = NaN;
                forOpti.nonOLVol{indexEl, indexStim} = NaN;
                %disp('Filling with NaNs')
        end

        forOpti.VTApath{indexEl,indexStim} = VTAPath;
        forOpti.stimAmp{indexEl,indexStim} = specStimAmplitude;
                

        
        
        
        
    end
    
    %estimate effective thresholds etc. for selected electrodes
    if sum(~isnan(cell2mat(forOpti.percSS(indexEl,:)))) > 1
        [minCurrentAmp, maxCurrentAmp, minDevCurrInd] = effective_threshold_estimation(indexEl, ...
            indexStim, forOpti, minSTNPerc, elLoopInd);
        effThresStruct.minCurrentAmp{indexEl} = minCurrentAmp; % effective threshold
        effThresStruct.maxCurrentAmp{indexEl} = maxCurrentAmp; %side-effect threshold. TODO: NaN for now
        effThresStruct.minDevCurrInd{indexEl} = minDevCurrInd; %index to stimulation closest to effective threshold
        effThresStruct.OLRAtMinCurr{indexEl}  = forOpti.OLR{indexEl, minDevCurrInd}; %overlap ratio at eff. thres.
        
    else
        effThresStruct.minCurrentAmp{indexEl} = NaN;
        effThresStruct.maxCurrentAmp{indexEl} = NaN;
        effThresStruct.minDevCurrInd{indexEl} = NaN;
        effThresStruct.OLRAtMinCurr{indexEl}  = NaN;
    end
end

%calc lowest (best) effective threshold and its corresponding electrode
[bestAmp, minDevElIndex] = min(cell2mat(effThresStruct.minCurrentAmp));
minDevEl = minDevElIndex-1; %index in convention 1-8, El in 0-7
bestAmp = round(bestAmp,1);


maxCurrVect = cell2mat(effThresStruct.maxCurrentAmp);
OLRVect = cell2mat(effThresStruct.OLRAtMinCurr);
maxCurrentAmp = maxCurrVect(minDevElIndex);
OLRAtMinCurr = OLRVect(minDevElIndex);



varargout{1} = minDevEl;
varargout{2} = bestAmp;
varargout{3} = maxCurrentAmp;
varargout{4} = OLRAtMinCurr;



resultStruct = rename_struct_endings(forOpti, side);
effThresStruct = rename_struct_endings(effThresStruct, side);


end

