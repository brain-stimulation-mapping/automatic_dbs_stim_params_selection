function [vol, varargout] = calc_vol(niiPath)
%Calculate volume of a nii file from its supplied path (with binarization)

niiLoaded = ea_load_nii(niiPath);

niiLoaded.img = round(niiLoaded.img); %binarize the voxel values

vol = numel(find(niiLoaded.img>0))*(niiLoaded.voxsize(1)*niiLoaded.voxsize(2)...
    *niiLoaded.voxsize(3)); %Multiply all positive voxels (ones) with the voxel size

varargout{1} =  niiLoaded;

end