function fullpath = check_folder(patientFolder, estimateInTemplate)
% control if folder for stimulation recommendations already exists, and
% then return the path to the stim rec file to be created

path =[patientFolder, filesep, 'stimulations',filesep, ea_nt(~estimateInTemplate)...
    filesep,'stim_recommendations/'];

if ~isfolder(path)
    mkdir(path)
end

count = 1; %for each created stim rec file, the final count is increased by one and added to the end of the file name
fullpath = fullfile(path, ['stim_recommendations',num2str(count),'.txt']);

%check how many recommendation files have already been created
while exist(fullpath, 'file') || isempty(fullpath)
    count = count+1;
    fullpath = fullfile(path, filesep,['stim_recommendations',num2str(count),'.txt']);
end

end