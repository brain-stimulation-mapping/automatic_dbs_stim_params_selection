function dataToGUIStruct = data_to_GUI(varargin)
%returns results data and settings data from the main code to 
%autostimulationGUI

side = varargin{1};

switch lower(side)
    case {'right','r',1, 'left', 'l', 2} %return results for the specific side
        dataToGUIStruct.bestEl = varargin{2};
        dataToGUIStruct.minBestCurr = varargin{3};
        dataToGUIStruct.stimLabelBase = varargin{4};
        
        dataToGUIStruct = rename_struct_endings(dataToGUIStruct, side);
        
    otherwise %return settings (no side selected, i.e., side = -1)
        dataToGUIStruct.patientFolder = varargin{2};
        dataToGUIStruct.stimType = varargin{3};
        dataToGUIStruct.estimateInTemplate = varargin{4};
        dataToGUIStruct.CWConvention = varargin{5};
        dataToGUIStruct.ringStim  = varargin{6};
end