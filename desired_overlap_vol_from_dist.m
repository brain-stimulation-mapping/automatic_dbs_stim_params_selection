function desired_overlap_vol = desired_overlap_vol_from_dist(distToSS, volSS)

%Calculates with how much the sweet spot should be stimulated, as a
%function of the SS-electrode distance. Transformation taken from trends in
%clinical data

volTrendSS = 87; 
lowerBoundary = 5; %mm3
upperBoundary = 40; %mm3
desired_overlap_vol = (41-7.5*distToSS)/volTrendSS*volSS;
desired_overlap_vol = max(desired_overlap_vol, lowerBoundary);
desired_overlap_vol = min(desired_overlap_vol, upperBoundary);

end
