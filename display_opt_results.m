function display_opt_results(varargin)
% display results from the optimization in the command window

ringStim = varargin{1};
side = varargin{2};
minCurrentAmp = varargin{3};
maxCurrentAmp = varargin{4};
OLRAtMinCurr = varargin{5};
minDevEl = varargin{6};

% have to change the electrode contact label if in ring stimulation
if ringStim 
   if minDevEl == 1 || minDevEl == 2 ||minDevEl == 3 
        minDevEl =  123;
   elseif minDevEl == 4 || minDevEl == 5 ||minDevEl == 6 
        minDevEl =  456;
   end  
end


switch lower(side)
    case {'r', 'right' ,1}
        sideText = 'Right: ';
    case {'l', 'left' ,2}        
        sideText = 'Left: ';
end

disp(' ')
disp(['Suggested Electrode Contact No., ', sideText, num2str(minDevEl)])
disp(['Full Therapeutic Effect Threshold, ', sideText, num2str(minCurrentAmp),' mA'])
disp(['Overlap Ratio at Full Therapeutic Effect Threshold, ', sideText, num2str(round(100*OLRAtMinCurr),2), ' %'])
disp(['Side Effect Threshold, ', sideText, num2str(maxCurrentAmp),' mA'])  


