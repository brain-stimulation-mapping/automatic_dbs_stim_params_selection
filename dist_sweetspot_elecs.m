function [distances, varargout] = dist_sweetspot_elecs(meanCoordsSS, coordsElecs)
% calculates the distances between each electrode center and the mean of
% the sweet spot


distances = NaN(size(coordsElecs,1),1);

% loop over all electrodes
for i = 1:size(coordsElecs,1)
    distances(i) = norm(meanCoordsSS - coordsElecs(i,:));
end


[min_dist,~] = min(distances); %calculate the distance for the closest electrode
varargout{1} = min_dist;
end