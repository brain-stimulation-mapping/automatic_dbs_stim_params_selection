function [minCurrentAmp, maxCurrentAmp, minDevCurrInd] = effective_threshold_estimation(...
    indexEl, indexStim, forOpti, minSTNPerc, elLoopInd)
% Estimates effective threshold by checking for which mA current of the 
% generated VTAs the minimum deviation to the desired overlap volume 
% is achieved. The estimate is refined by linear interpolation 
% between the initial estimate point and its neighbor 


% calc the deviations from the desired overlap volume for each mA current
percSS = cell2mat(forOpti.percSS(indexEl,:));
deviationsSS = percSS - minSTNPerc/100;
[~, minDevCurrInd] = min(abs(deviationsSS)); %find index of smallest deviation

% find current corresponding to minimum deviation index
stimAmps = cell2mat(forOpti.stimAmp(indexEl,:));
minCurrentAmpInit = stimAmps(minDevCurrInd);

try

    boundaryCase = 0; %will be one if initial guess first or last index in array
    if deviationsSS(minDevCurrInd) < 0 && minDevCurrInd == max(elLoopInd) %can't interpolate beyond last element
         minCurrentAmp = minCurrentAmpInit;
         boundaryCase = 1;
    elseif deviationsSS(minDevCurrInd) > 0 && minDevCurrInd == 1 %can't interpolate before first element
         minCurrentAmp = minCurrentAmpInit;
         boundaryCase = 1;                        
    elseif deviationsSS(minDevCurrInd) < 0 && deviationsSS(minDevCurrInd +1) > 0 %initial estimate too low mA
        y_2 = percSS(minDevCurrInd+1); % overlap perc for the closest mA point higher than initial guess (to the right)
        x_2 = stimAmps(minDevCurrInd + 1); % mA for the closest mA point higher than initial guess                            
    elseif deviationsSS(minDevCurrInd) > 0 && deviationsSS(minDevCurrInd - 1)<0 %initial estimate too high mA
        y_2 = percSS(minDevCurrInd-1); % overlap perc for the closest mA point lower than initial guess (to the left)
        x_2 = stimAmps(minDevCurrInd - 1); % mA for the closest mA point lower than initial guess                         
    end

    if ~boundaryCase %no interpolation done if boundary case
        y_1 = percSS(minDevCurrInd); %overlap perc for initial guess

        y_w = minSTNPerc/100; %desired overlap perc
        x_1 = minCurrentAmpInit; %initial mA  estimate


        deltaY = (y_w-y_1)/(y_2-y_1); % fraction of how far away the desired overlap vol is from the initial guess and the next data point
        minCurrentAmp = x_1 + deltaY*(x_2-x_1); % have to step equally (percentually) far on the x-axis as on the y-axis to find best estimate for 0-crossing of deviatoin                  
        minCurrentAmp = round(minCurrentAmp,1); % round off to 1 decimal precision
    end
catch errorObj
    %minCurrentAmp = NaN;
    minCurrentAmp = minCurrentAmpInit;
    warning(errorObj.message)
end

maxCurrentAmp = NaN; %TODO: only for now, should be estimated from nonOverlapVol though
%TODO interpolation for refinement of overlap ratio at effective threshold

end