function [minCurrentAmp, maxCurrentAmp, OLRAtMinCurr ] ...
    = get_IAS_results_for_lead(minCurrentAmpVect, maxCurrentAmpVect, OLRAtMinCurrVect)
% gather results for entire lead, calculating effective, side-effects
% thresholds and overlap ratio at effective threshold for the best electrode

minCurrVect = cell2mat(minCurrentAmpVect);
maxCurrVect = cell2mat(maxCurrentAmpVect);
OLRVect = cell2mat(OLRAtMinCurrVect);
[minCurrentAmp, minCurrentIndex] = min(minCurrVect);
maxCurrentAmp = maxCurrVect(minCurrentIndex);
OLRAtMinCurr = OLRVect(minCurrentIndex);