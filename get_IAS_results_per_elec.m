function [minCurrentAmp, OLR_at_min_curr, maxOLR,currentAtMaxOLR, maxCurrentAmp, varargout]...
         = get_IAS_results_per_elec(evalMetric, minOverlapVol, maxNonOverlapVol, ...
                           mA_full, OLR_full, V_OL_full, V_non_OL, varargin)
% get results such as effective threshold from the data from the IAS model

if exist('varargin','var') && numel(varargin)>0
    distToSS = varargin{1};
    volSS = varargin{2};
    minOverlapVol = desired_overlap_vol_from_dist(distToSS,volSS);
end


if strcmp(evalMetric,'Minimum sweetspot volume activation') || ...
        strcmp(evalMetric,'Minimum percentage of sweetspot activation')
   
    % find minimum (eff. thres.) and max (side-effect thres.) and their
    % indices
    [~, minOverlapVolInd] =  min(abs(V_OL_full-minOverlapVol));
    minCurrentAmp = round(mA_full(minOverlapVolInd),1);
    [~, maxNonOverlapVolInd] =  min(abs(V_non_OL-maxNonOverlapVol));
    maxCurrentAmp = round(mA_full(maxNonOverlapVolInd),1);

elseif strcmp(evalMetric, "Overlap ratio maximization within amplitude range")

   minCurrentAmp = minAmp;
   maxCurrentAmp = maxAmp;

   [~, minOverlapVolInd] =  min(abs(mA_full-minCurrentAmp));
   [~, maxNonOverlapVolInd] =  min(abs(mA_full-maxCurrentAmp));
end

% get SS overlap volume, non-overlap volume etc.
V_OL_at_min_curr = round(V_OL_full(minOverlapVolInd),1);
V_non_OL_at_min_curr = round(V_non_OL(minOverlapVolInd),1);
OLR_at_min_curr = V_OL_at_min_curr/(V_OL_at_min_curr+V_non_OL_at_min_curr);
V_non_OL_at_max_curr = round(V_non_OL(maxNonOverlapVolInd),1);

%maximize overlap ratio only within given/computed mA current range
OLR_restricted = OLR_full;
OLR_restricted(1:minOverlapVolInd-1) = 0;
OLR_restricted(maxNonOverlapVolInd:end) = 0;

% find max overlap ratio and its corresponding mA current
if any(OLR_restricted)
   [~, maxOverlapRatioVolInd] =  max(OLR_restricted); 
   maxOLR = round(OLR_restricted(maxOverlapRatioVolInd),2); %
   currentAtMaxOLR = round(mA_full(maxOverlapRatioVolInd),1);
else
   maxOLR = NaN;
   currentAtMaxOLR = NaN;
end

 varargout{1} = V_OL_at_min_curr;
 varargout{2} = V_non_OL_at_max_curr;
end
