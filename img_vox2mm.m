function coords_mm =  img_vox2mm(niiLoaded)
% transform nii.img to mm coords

[xx,yy,zz] = ind2sub(size(niiLoaded.img),(find(niiLoaded.img>0))); 
coords_vox = [xx,yy,zz];

coords_mm = ea_vox2mm(coords_vox, niiLoaded.mat);

end