
function [r1_full, mA_full, OLR_full, V_OL_full, V_non_OL] = instant_Analytical_Spheres(varargin)
% IAS model for estimating VTA-sweet spot overlap volumes etc.

%Following assumptions used:
%-VTAs and sweetspot perfect spheres
%-Center of mass (CoM) of directional VTAs move in the direction from the
%   center of the ring electrodes to the electrode surface, with an assumed
%   "speed ratio" in mm per mm VTA radius increase
%


V_SP = varargin{1}; %volume of sweet spot
elCoords = varargin{2};
elNmbr = varargin{3}; %In 0-7 convention

meanSPcoords = varargin{4}; %Center of sweet spot

%check if side is provided
if numel(varargin)>=5
    side = varargin{5}; %1 or 2 
else
    side = -1;
    disp('ERROR: no side selected')
    return
end

%check if ring stimulation is provided
if numel(varargin)>=6
    ringStim = varargin{6}; %TODO, 0, 1
else
    ringStim = 0; %default directional stim
end

%check if value for speed ratio is provided
if numel(varargin)>=7
    speedRatio = varargin{7}; 
else
    speedRatio = 0.1; %default value. TODO find empirical/analytical value for this
end

% check if stimulate from center of ring or surface of electrode
if numel(varargin)>=8
    fromRingCenter = varargin{8};% ==1 gives a false correction
else
    fromRingCenter = 0;
end

%check if all contacts should be modeled 
if numel(varargin)>=9
    allContactsBool = varargin{9};
else
    allContactsBool = 0;
end


% set label for ring stimulation according to level
if ringStim
    if elNmbr == 1 || elNmbr == 2 || elNmbr == 3
        ringStimLab = 'lower';
    elseif elNmbr == 4 || elNmbr == 5 || elNmbr == 6
        ringStimLab = 'upper';
    else
        ringStimLab = 'none';
    end
else
    ringStimLab = 'none';
end

elNmbrInd = elNmbr+1; %elNmbrInd in 1-8 convention; elNmbr in 0-7

%check if the current electrode is directional
if (elNmbr == 1 || elNmbr == 2 || elNmbr == 3 || elNmbr == 4 || elNmbr == 5 ...,
        || elNmbr == 6) && ~ringStim 
   directional = 1; 
else
   directional = 0; 
end

%get center of ring levels
lowerRingCenter = mean(elCoords{side}(2:4,:));
upperRingCenter = mean(elCoords{side}(5:7,:));

%select center coords to use for electrode
if strcmp(ringStimLab, 'lower')
   elCenter = lowerRingCenter; 
elseif strcmp(ringStimLab, 'upper')
   elCenter = upperRingCenter;    
else
   elCenter = elCoords{side}(elNmbrInd,:);
end
    
% calculate trajectory from center of ring to electrode surface if
% directional electrode
if directional %&& (side == 1) 
   if (elNmbr == 1 || elNmbr == 2 ||elNmbr == 3)

       elTraj = elCoords{side}(elNmbrInd,:)-lowerRingCenter(:)';

   elseif (elNmbr == 4 || elNmbr == 5 ||elNmbr == 6)
     
       elTraj = elCoords{side}(elNmbrInd,:)-upperRingCenter(:)';

   end
end

%calc initial distance between elec and sweet spot
d_start = norm(elCenter-meanSPcoords);

% calc radius of sweet spot
r2 = (3.*V_SP./(4*pi)).^(1/3); 


extra = 3; %mm radius after full overlap to plot


% specify empirical transform equation from radius to current
radiusToCurrent = @(r) 0.4279.*r + 0.3595.*r.^2; %from pat 0215
%radiusToCurrent = @(r) 0.4552.*r + 0.3552.*r.^2; %from pat 0187
%currentToRadius = @(I) 0.6818 + 0.6185*I-0.02183*I.^2  

% open new figure or get handle to one already created
if allContactsBool && elNmbr ~= 0 
    hWithSub = gcf;
else
    hWithSub = figure;
end

%set position of figure
pos = get(hWithSub,'pos');
set(hWithSub,'pos',[pos(1) pos(2) 1500 500])


if ~directional % if omni-directional
        d = d_start; %initial distance
        
        if d<r2 %SP envelopes electrode center

            bef_OLR = ones(1,100); %full overlap
            r1 = linspace((r2-d),d+r2); %VTA radius values for which surfaces intersect
            
        else % r2 >= d (no overlap at r2=0)
            bef_OLR = zeros(1,100); %no overlap 
            bef_OL = zeros(1,100); % no overlap
            r1 = linspace(d-r2,d+r2); %VTA radius values for which surfaces intersect
        end
        
        
        %specify formulas for overlap volumes and VTA volumes
        V_OL =@(r1) pi./(12.*d).*((r1+r2-d).^2).*(d.^2+2.*d.*(r1+r2)-3.*(r1-r2).^2);
        V_VTA =@(r1)4.*pi.*r1.^3/3;
        
        %r1 values for which no surface intersection occurs
        r1_start = linspace(0,r1(1));
        r1_after = linspace(d+r2,d+r2+extra);
        
        if d<r2 %SP envelopes electrode center
           bef_OL = V_VTA(r1_start); %overlap vol is equal to VTA volume
        end
        
        r1_full = [r1_start, r1, r1_after]; %put together all parts of the r1 array
        OLR = V_OL(r1)./V_VTA(r1); % calc overlap ratios for intersecting volumes
        OLR_aft_OL = max(V_OL(r1))./V_VTA(r1_after); %calc overlap ratios when entire sweet spot is overlapped
        OLR_full = [bef_OLR, OLR, OLR_aft_OL]; %put together all parts of the overlap ratio array
        
        OL_after = linspace(V_SP, V_SP, numel(r1_after)); %overlap array after full overlap with sweet spot
        mA_full = radiusToCurrent(r1_full); %calc currents from radius-to-current transform
        V_OL_full = [bef_OL, V_OL(r1), OL_after]; %full overlap volume array
        V_non_OL = V_VTA(r1_full)-V_OL_full; %full non-overlap volume array
else %directional -> include d dependence on r1 since CoM moves

    d = d_start; %initial distance between elec and sweet spot (at r1 = 0)
    rMax = d+r2+extra; %define maximum radius to include
    r1_full = linspace(0,rMax,300); %array of VTA radii from 0 to the maximum radius
    mA_full = radiusToCurrent(r1_full); % currents corresponding to r1 values
    
    %calc distances between elec and sweetspot as a function of the VTA
    %radius
    d_vect = d_dir(r1_full, elTraj, meanSPcoords, elCenter, speedRatio, fromRingCenter);
    
    %calculate overlap volume etc for each VTA radius value
    for i = 1:numel(r1_full)
       d = d_vect(i); 
       r1 = r1_full(i); 
        
       if (d+r1 < r2) %VTA fully enclosed in sweetspot
           OLR(i) = 1; %overlap ratio = 1
           OL(i) = V_VTA_dir(r1); %overlap volume equal to vol of VTA
           nonOL(i) = 0; %non-SS activation vol is 0
       elseif (r1+r2 < d) %No overlap between VTA and SP
           OLR(i) = 0; %overlap ratio = 0
           OL(i) = 0; %no overlap volume with SS
           nonOL(i) = V_VTA_dir(r1); %all stimulation is of non-sweet spot tissue
       elseif (r1 > r2+d) %SS fully enclosed by VTA
           OLR(i) = V_SP/V_VTA_dir(r1); %overlap ratio equal to vol of sweet spot div by VTA vol
           OL(i) = V_SP; %entire sweet spot overlapped 
           nonOL(i) = V_VTA_dir(r1)-V_SP; %non-SS stim vol is VTA vol minus vol of sweet spot
       else %SP and VTA partially overlap
           OL(i) = V_OL_dir(r1,d,r2); %use analytical formula to calc overlap vol
           OLR(i) = OL(i)/V_VTA_dir(r1); %overlap ratio from overlap vol and VTA vol
           nonOL(i) = V_VTA_dir(r1)-OL(i); %non-SS stim vol is VTA vol minus overlap vol
       end
    end
    OLR_full = OLR;
    V_OL_full = OL;
    V_non_OL = nonOL;

end

%set plot markers and legend entries
if allContactsBool && ~ringStim
    markers = {'-','--','.','-.','o','+','*',':'};
    for i = 1:8
        entries{1,i} = ['El. No.: ',num2str(i-1)];
    end
elseif ringStim
    markers = {'-','--','.','-.','o','+','*',':'};
    entries{1,1} = ['El. No.: 0'];
    entries{1,2} = ['El. No.: 1-2-3'];
    entries{1,3} = ['El. No.: 4-5-6'];
    entries{1,4} = ['El. No.: 7'];
else
    markers ={'-', '-','-','-','-','-','-','-',};
    entries{1,1} = ['El. No.: ',num2str(elNmbr)];
end

%plot overlap ratio vs stim amp
subplot 131 %figure(hOLR)
plot(mA_full, OLR_full,markers{elNmbrInd});
hold on

%plot overlap volume vs stim amp
subplot 132 %figure(hVolOL)
plot(mA_full, V_OL_full,markers{elNmbrInd});
hold on

%plot non-overlap vs stim amp
subplot 133% figure(hVolNonOL)
%V_non_OL = V_VTA(r1_full);
plot(mA_full, V_non_OL,markers{elNmbrInd});     
hold on


%set axes labels, limits and legend for OLR plot
subplot 131
xlabel('Stimulation current [mA]')
%xlabel('VTA radius [mm]')
ylabel('Overlap ratio [-]')
xlim([0,8])
leg = legend(entries);
set(leg,'location','best')

if (side == 1)
    title('Mathematical model of overlap ratio, right')
    %title('Mathematical model of overlap ratio')
elseif (side == 2)
    title('Mathematical model of overlap ratio, left')
end


%set axes labels, limits and legend for overlap vol plot

subplot 132
xlabel('Stimulation current [mA]')
%xlabel('VTA radius [mm]')
ylabel('Volume of sweetspot stimulation [mm^3]')
xlim([0,8])

if (side == 1)
    title('Mathematical model of overlap volume, right')
    %title('Mathematical model of overlap volume')
elseif (side == 2)
    title('Mathematical model of overlap volume, left')
end
leg = legend(entries);
set(leg,'location','best')



%set axes labels, limits and legend for non-overlap plot
subplot 133
xlabel('Stimulation current [mA]')
%xlabel('VTA radius [mm]')
ylabel('Volume of non-sweetspot stimulation [mm^3]')
xlim([0,8])

if (side == 1)
    title('Mathematical model of non-overlap volume, right')
    %title('Mathematical model of non-overlap volume')
elseif (side == 2)
    title('Mathematical model of non-overlap volume, left')
else
    title('Mathematical model of non-overlap volume')
end
leg = legend(entries);
set(leg,'location','best')

curAxis = axis;
curAxis(3) = 0;
axis(curAxis);


end

%analytical overlap function between to spheres of radii r1 (VTA) and r2
%(SS), with a distance d between their centers
function V_OL = V_OL_dir(r1,d,r2)
    V_OL = pi./(12.*d).*((r1+r2-d).^2).*(d.^2+2.*d.*(r1+r2)-3.*(r1-r2).^2);
end

%formula for vol of sphere given radius
function V_VTA = V_VTA_dir(r1)
    V_VTA = 4.*pi.*r1.^3/3;
end

% calc distance between VTA CoM and mean of SS as a function of VTA radius
function d = d_dir(r1, elTraj, meanSPcoords, elCenter, speedRatio, fromRingCenter)
    %"speedRatio" in mm/mm (VTA CoM displacement/VTA radius increase)
    
    
    displacement = speedRatio.*r1; %absolute displacement of CoM from initial pos
    trajUnitVector =  elTraj./norm(elTraj); %unit vector in trajectory direction
    
    % calc CoM of VTA for given radii
    if fromRingCenter %if initial CoM is in ring center (0 by default)
        VTACenter = elCenter-elTraj+0.0*elTraj+(trajUnitVector(:).*displacement)'; 
    else %if initial CoM starts from electrode surface
        VTACenter = elCenter+(trajUnitVector(:).*displacement)'; %start from initial ...
        %CoM position and move in the trajectory direction a distance of
        %"displacement" mm
    end
    
    %calculate distance between VTA CoM and center of sweet spot
    for i = 1:size(VTACenter,1)
        d(i)=norm(meanSPcoords-VTACenter(i,:));
    end
    
end