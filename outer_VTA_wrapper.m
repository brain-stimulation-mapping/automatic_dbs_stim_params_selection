
function outer_VTA_wrapper(patientFolder, stimLabelBase, side, cathode, stimAmplitude, anode, stimType, estimateInTemplate, CWConvention, ringStim)

% sends VTA settings to wrapper for VTA generation

for indexCathode = 1:numel(cathode) %loop over all selected cathodes (elec)
    for indexAmplitude = 1:numel( stimAmplitude ) %loop over all stimulation amplitudes
        
        cathodeNum = cathode(indexCathode);
        
        % ring stim data for lower ring stored in elec 2, for upper in elec
        % 5. Have to divide the current and change the label if ring stim
        if ringStim && cathodeNum == 2 %lower ring
            cathodeNum = 1:3;
            elecText = '123';
        elseif ringStim && cathodeNum == 5 %upper ring
            cathodeNum = 4:6;
            elecText = '456';
        else
            elecText = num2str(cathode(indexCathode));
        end
        
                
        % if clockwise convention, switch indices 2 and 3, as well as 5 and 6
        if exist('CWConvention','var') && CWConvention           
            
            if cathode(indexCathode) == 2
                cathodeNum = 3;
            elseif cathode(indexCathode) == 3
                cathodeNum = 2;                
            elseif cathode(indexCathode) == 5
                cathodeNum = 6;   
            elseif cathode(indexCathode) == 6
                cathodeNum = 5;   
            end
        end
        
        
        stimName = [ stimLabelBase num2str( stimAmplitude( indexAmplitude )), 'mA_', elecText, 'Elec' ];
        
        %check if VTA already exists, in that case no new one is generated
        if estimateInTemplate && exist([patientFolder, filesep, 'stimulations',...
                filesep,ea_nt(0), stimName, filesep, 'vat_right.nii'],'file') ...
            && strcmp(side, 'r') 
            disp(['VTA already exists, R: ',num2str( stimAmplitude( indexAmplitude )),...
                'mA_', elecText, 'Elec'])

        elseif estimateInTemplate && exist([patientFolder, filesep, 'stimulations', ...
                filesep,ea_nt(0), stimName, filesep, 'vat_left.nii'],'file') ...
            && strcmp(side, 'l') 
            disp(['VTA already exists, L: ',num2str( stimAmplitude( indexAmplitude )), 'mA_', elecText, 'Elec'])
        else %VTA doesn't exist, so generate one
            try
                ea_genvat_wrapper( ...
                    patientFolder, ...
                    stimName,...
                    side,...
                    cathodeNum,...
                    stimAmplitude( indexAmplitude ),...
                    anode, ...
                    stimType, ...
                    estimateInTemplate);
            catch errorObj
                %msgbox(['Error in VTA calc. for: ', num2str( stimAmplitude( indexAmplitude )), 'mA_', elecText, 'Elec', upper(side) ])
                warning(errorObj.message) %print failed VTAs to file?
            end
        end
    end
end


% what stimulation settings are being used?
