function plot_overlap_results(ringStim, stimAmp, OLR, percStimSS, nonOLVol,...
                              volumeSS, figureHandle)
% plot overlap ratios, overlap volumes and non-overlap volumes results
                          
figure(figureHandle)

markers = {'o','+','*','.','x','_','s','d'};

if ~ringStim

    for i = 1:8
        entries{1,i} = ['El. No.: ',num2str(i-1)]; %entries for legends
    end

    plotIndices = 1:8;
else

    entries{1,1} = 'El. No.: 0';
    entries{1,2} = 'El. No.: 1-2-3';
    entries{1,3} = 'El. No.: 4-5-6';
    entries{1,4} = 'El. No.: 7';

    plotIndices = [0 2 5 7]+1; %indices where ring stim values can be found
end



for i = plotIndices
    
    %plot overlap ratios vs stim amplitude
    subplot 131 
    plot(cell2mat(stimAmp(i,:)), cell2mat(OLR(i,:)),markers{i});
    hold on
    
    %plot SS overlap vol vs stim amplitude
    subplot 132 
    plot(cell2mat(stimAmp(i,:)), cell2mat(percStimSS(i,:))*volumeSS,markers{i});
    hold on
    
    %plot SS non-overlap vol vs stim amplitude
    subplot 133
    plot(cell2mat(stimAmp(i,:)), cell2mat(nonOLVol(i,:)),markers{i});
    hold on

end

%set legend and axes labels
for i = 1:3
    subplot(1,3,i)
    leg = legend(entries);
    xlabel('Stimulation current [mA]')

    if i == 1
        ylabel('Overlap ratio [-]')
    elseif i == 2
        ylabel('Volume of sweetspot stimulation [mm^3]')
    elseif i == 3
        ylabel('Volume of non-sweetspot stimulation [mm^3]')
    end

    set(leg,'location','best')
end



end 