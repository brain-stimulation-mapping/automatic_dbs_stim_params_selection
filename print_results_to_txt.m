function varargout = print_results_to_txt(patientFolder, side, minDevEl,minCurrentAmp,...
    maxCurrentAmp, OLRAtMinCurr, estimateInTemplate, varargin)
% print optimization results to a txt file (stim_recommendationXX.txt)

% check if inputs contain path to already initiated stim rec file
if numel(varargin) >=1 && ~isempty(varargin{1})
    fullpath = varargin{1};
    fid = fopen(fullpath, 'a'); 
    %fgetl(fid)
else %otherwise the file has to be created
    
    %create and open file with a name that doesn't already exist
    fullpath = check_folder(patientFolder, estimateInTemplate); 
    fid = fopen(fullpath, 'a');
    fprintf(fid,['RESULTS', '\n']);
end



switch lower(side)
    case {'r', 'right', 1}
        sideText = 'Right: ';
    case {'l', 'left', 2}
        sideText = 'Left: ';
end


%print results to file
fprintf(fid,['\n','Suggested Electrode Contact No., ', sideText , num2str(minDevEl), '\n']);

%     if (strcmp(optiMethod, 'Instant analytical spheres (Nordenstroem)'))

fprintf(fid, ['Min current amplitude for therapeutic effect, ', sideText , num2str(minCurrentAmp),' mA', '\n']);
fprintf(fid,['Max current amplitude before side-effects, ', sideText , num2str(maxCurrentAmp),' mA', '\n']);
%fprintf(fid, ['Overlap volume at min effective current, ', sideText  num2str(V_OL_at_min_curr), ' mm^3', '\n']);
fprintf(fid, ['Overlap ratio at min effective current, ', sideText  num2str(round(100*OLRAtMinCurr,1)), ' %%', '\n']);
%fprintf(fid, ['Non-overlap volume at max effective current, ', sideText  num2str(V_non_OL_at_max_curr), ' mm^3', '\n', '\n']);
%     else
%         
%         fprintf(fid,['Suggested Current Amplitude, ', sideText , num2str(minCurrentAmp),' mA', '\n']);
%         fprintf(fid,['Best Overlap Ratio, ', sideText , num2str(bestOverlap*100),' %%','\n','\n']);
%     end


fclose(fid);

varargout{1} = fullpath; %path to file
end
