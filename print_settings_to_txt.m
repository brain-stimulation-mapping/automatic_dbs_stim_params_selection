function print_settings_to_txt(rightHemisphere, leftHemisphere, stimLabelBase,...
    spNames, minOverlapVol, maxNonOverlapVol, estimateInTemplate, ringStim,...
    optiMethod, evalMetric, varargin)
%print the settings used to a txt file (stim_recommendationXX.txt)

% check if inputs contain path to already initiated stim rec file
if  numel(varargin) >=1 && ~isempty(varargin{1})
    fullpath = varargin{1};
else %otherwise the file has to be created 
    fullpath = check_folder(patientFolder, estimateInTemplate);     
end

fid = fopen(fullpath, 'a');

%print setting to file
fprintf(fid,['\n', '\n', 'SETTINGS USED', '\n','\n']);

if rightHemisphere && leftHemisphere
    fprintf(fid,['Side: Left and right', '\n']);
elseif ~rightHemisphere && leftHemisphere
    fprintf(fid,['Side: Left', '\n']);
elseif rightHemisphere && ~leftHemisphere
    fprintf(fid,['Side: Right', '\n']);
elseif ~rightHemisphere && ~leftHemisphere
    fprintf(fid,['Side: None','\n']);
end


fprintf(fid,['Stimulation label: ', stimLabelBase ,'\n']);
fprintf(fid,['Sweetspot file, right: ', spNames.Right,'\n']);
fprintf(fid,['Sweetspot file, left: ', spNames.Left,'\n']);

% if (strcmp(optiMethod, 'Instant analytical spheres (Nordenstroem)'))
    fprintf(fid,['Min sweetspot overlap volume, right: ', num2str(minOverlapVol.R), ' mm^3','\n']);
    fprintf(fid,['Min sweetspot overlap volume, left: ', num2str(minOverlapVol.L), ' mm^3','\n']);
    fprintf(fid,['Max non-overlap volume, right: ', num2str(maxNonOverlapVol.R), ' mm^3','\n']);
    fprintf(fid,['Max non-overlap volume, left: ', num2str(maxNonOverlapVol.L), ' mm^3','\n']);
% else
%     fprintf(fid,['Desired overlap ratio, right: ', num2str(optimalOR.R),'\n']);
%     fprintf(fid,['Desired overlap ratio, left: ', num2str(optimalOR.L),'\n']);
% end

% fprintf(fid,['Minimum amplitude: ', num2str(minAmp),'\n']);
% fprintf(fid,['Maximum amplitude: ', num2str(maxAmp),'\n']);
% fprintf(fid,['Step size: ', num2str(stepSize),'\n']);
fprintf(fid,['Estimate in template space: ', num2str(estimateInTemplate),'\n']);
fprintf(fid,['Include ring stimulation: ', num2str(ringStim),'\n']);
fprintf(fid,['Optimization method: ', optiMethod,'\n']);
fprintf(fid,['Evaluation metric: ', evalMetric,'\n']);


fprintf(fid,['Generated: ' datestr(datetime),'\n']);

fclose(fid);
end