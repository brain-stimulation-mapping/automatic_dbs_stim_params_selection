function renamedStruct = rename_struct_endings(origStruct, side)
%add R or L at the end of all fields in a struct

origNames = fieldnames(origStruct);

renamedStruct = origStruct;

switch lower(side)
    case {'right','r',1}
        addEnd = 'R';
    case {'left', 'l', 2}
        addEnd = 'L';        
end

for i = 1:numel(origNames)
    
    newName = origNames{i};
    newName = [newName, addEnd];
    [renamedStruct.(newName)] = renamedStruct.(origNames{i});
    renamedStruct = rmfield(renamedStruct,origNames{i});
    
end


end

