function [selElecsInd, minDevEl] = select_elecs(nmbrOfSelected, distances)
%select the nmbrOfSelected electrodes closest to the sweet spot

[~,bestIndices] = sort(distances);
selElecsInd = bestIndices(1:nmbrOfSelected)-1; %-1 to get to 0-7 convention
minDevEl = selElecsInd(1);

